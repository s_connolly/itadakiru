# Itadakiru 1

## Style and Script
Itadakiru uses [semantic ui](https://semantic-ui.com) and some Custom CSS to create a simplistic look.

### Custom CSS Workflow
The front-end workflow utilizes Laravel mix to process the css and javascript. All mix settings can be fount on webpack.mix.js. 
- The source files can be found on resource/assets folder which is Laravel's default src location. 
- The sass folder contains the custom styles and broken down into folders. Base - mostly includes variables, mixin and global styles which are not compiled(except global.scss). Components - styles that are not included in semantic ui and some semantic override. Layouts - are mostly layout related styles.
- All files are imported in app.scss which is then compiled into a single app.css inside public/css folder

### JS Workflow
Javascript also uses Laravel mix in compiling vue and js files.
- app.js is compiled into app.js inside public/js folder.
- all global js are inside page-scripts.js which is initialized inside app.js
- local and specific js can be included inside blade files themselves. 3rd party library can be included in page-script-before section and local script can be included in page-script-after section

## Notifications
Sending shipments notifies customer thru SMS. Itadakiru uses nexmo notifications under the hood.
- Notification message can be set in Notifications/ShippingSent.php
- ShippingSent.php are tied to shipments model which uses notifiable trait
- Sending SMS are queue which requires a worker
