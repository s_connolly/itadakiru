<?php

return [
    'shippingCreated' => "Your order for :productName has been received. Will update shortly with tracking information",
    'shippingSent' => ":productName just left and should be with you inside of a week. Tracking Number: :trackingNumber.\nFor Orders to the USA, Fedex. For Orders to Asia, SF Express"
];
