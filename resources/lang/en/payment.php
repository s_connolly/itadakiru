<?php

return [
    /* Payment */
    'payment_title' => 'Payment Information',
    'item' => 'Item',
    'amount' => 'Amount',

    'uses_bitcoin' => "Payment is made with Bitcoin, Upon order completion you will be redirected to a confirmation page and recieve a text message on your phone",

    'refresh' => "Don't refresh the page",
    'seconds_to_confirm' => 'Most wallets take 10 seconds to Confirm',
    'minutes_to_exchange' => 'BTC sent from exchanges may take 10-15 minutes',
    /* Owed */
    'owed' => 'BTC is still owed',
    'send_remaining' => "Please send the remaining amount and click 'Payment Sent'",
    'send_again' => "If you believe this to be an error please click 'Payment Sent' again",
    /* Confirmed */
    'confirmed_title' => 'Thank you for your order',
    'shipping_time' => 'Will ship within 48 hours to'
];
