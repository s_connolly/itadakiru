<?php

return [
    'usa' => 'USA',
    'china' => 'China',
    'hongkong' => 'Hong Kong',
    'japan' => 'Japan',
    'macau' => 'Macau',
    'taiwan' => 'Taiwan'
];
