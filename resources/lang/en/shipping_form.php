<?php

return [

    'title' => 'Shipping Details',
    'name' => 'Name',
    'country' => 'Country',
    'city' => 'City',
    'address_1' => 'Address Line 1',
    'address_2' => 'Address Line 2',
    'postal' => 'Postal',
    'phone' => 'Phone',
    'phone_tracking' => 'Tracking will be sent via SMS',
    'shipping_arrival' => 'Please Allow 1 Week for Arrival',
    'shipping_included' => 'Shipping & Import Duties Included',
    'shipping_countries' => 'Contact us for Shipping to Other Countries'
];