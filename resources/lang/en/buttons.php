<?php

return [

    'buy' => 'Buy Now',
    'sold_out' => 'Sold Out',
    'submit' => 'Submit',
    'cancel' => 'Cancel',
    'payment_sent' => 'Payment Sent',
    'show_qr' => 'Show QR',
    'continue_shopping' => 'Continue Shopping',
    'go_to_shop' => 'Go to shop',
    'login' => 'Login',
    'adjust_stock' => 'Adjust Stock',
    'delete' => 'Delete',
    'sent' => 'Sent',
    'register' => 'Register',
    'review_add' => 'Add Review'
];