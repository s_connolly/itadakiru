<?php

return [
    'menu' => 'Menu',
    'manage' => 'Manage',
    'pending' => 'Pending Shipment',
    'sent' => 'Sent Shipment',
    'no_products' => 'There are no products',
    'no_pending' => 'There are no pending shipments',
    'no_sent' => 'There are no sent shipments',
    'delete_confirm' => 'Are you sure you want to delete',
    'gemini_key' => 'Gemini Keys'
];
