<?php

return [

    'btc_api_error' => 'Error with API calls & setting BTC prices. Please check internet connection and reload page',
    'btc_api_error_e1' => 'Error, Unable to generate BTC address, please check internet connection and retry.',
    'btc_api_error_e2' => 'Error, Unable to generate BTC address, please check internet connection and retry.',
    'btc_cypher_error' => 'Error with payment checking, Please check internet connection and try again. DO NOT RELOAD PAGE.',
    'created' => 'was created',
    'deleted' => 'was deleted',
    'keySaved' => 'Your new keys were saved',
    'not_saved' => 'Data not saved.',
    'notSaved' => 'There was an error saving your keys',
    'shipmentSent' => 'Shipment sent',
    'updated' => 'was updated',
];
