<?php

return [
    'usa' => '美國',
    'china' => '中國',
    'hongkong' => '香港',
    'japan' => '日本',
    'macau' => '澳門',
    'taiwan' => '台灣'
];
