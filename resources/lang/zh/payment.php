<?php

return [
    /* Payment */
    'payment_title' => '付款資訊',
    'item' => '條目',
    'amount' => '數額',

    'uses_bitcoin' => "若使用比特幣付款，訂單完成時，你會被轉接至確認頁面並收到手機短訊。",

    'refresh' => "請不要刷新頁面",
    'seconds_to_confirm' => '大多數錢包需10秒鐘確認',
    'minutes_to_exchange' => '從交易所發送的比特幣可能需要10至15分鐘',
    /* Owed */
    'owed' => '仍欠比特幣',
    'send_remaining' => "請發送剩餘數額並點擊「付款已發送」。",
    'send_again' => "若你認為這是一個錯誤消息，請再次點擊「付款已發送」。",
    /* Confirmed */
    'confirmed_title' => '感謝你的訂單',
    'shipping_time' => '我們將在48小時內發貨至'
];
