<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密碼須至少6個字符且兩次輸入密碼須相符。',
    'reset' => '你的密碼已重置！',
    'sent' => '我們已將你的密碼重置鏈接通過電子郵件發送給你！',
    'token' => '該密碼重置 token 無效。',
    'user' => "我們找不到有該電子郵件地址的用戶。",

];
