<?php

return [
    'menu' => '功能表',
    'manage' => '管理',
    'pending' => '待定發貨',
    'sent' => '已發貨',
    'no_products' => '無產品',
    'no_pending' => '無待定發貨',
    'no_sent' => '無已發貨',
    'delete_confirm' => '你確定要刪除嗎？',
    'gemini_key' => 'Gemini 密鑰'
];
