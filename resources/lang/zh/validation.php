<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute 必須被接受。',
    'active_url'           => ':attribute 非有效 URL。',
    'after'                => ':attribute 必須為 :date 之後的一個日期。',
    'after_or_equal'       => ':attribute 必須為 :date 之後或與其等同的一個日期。',
    'alpha'                => ':attribute 只許包含字母。',
    'alpha_dash'           => ':attribute 只許包含字母、數字、破折號和下劃線。',
    'alpha_num'            => ':attribute 只許包含字母和數字。',
    'array'                => ':attribute 必須為一個陣列。',
    'before'               => ':attribute 必須為 :date 之前的一個日期。',
    'before_or_equal'      => ':attribute 必須為 ：date 之前或與其等同的一個日期。',
    'between'              => [
        'numeric' => ':attribute 必須在 :min 與 :max 之間。',
        'file'    => ':attribute 必須在 :min 與 :max 千字節之間。',
        'string'  => ':attribute 必須在 :min 與 :max 字符之間。',
        'array'   => ':attribute 必須在 :min 與 :max 項之間。',
    ],
    'boolean'              => ':attribute 欄必須為真或假。',
    'confirmed'            => ':attribute 確認不匹配。',
    'date'                 => ':attribute 非有效日期。',
    'date_format'          => ':attribute 與格式 :format 不匹配。',
    'different'            => ':attribute 與 :other 必須不同。',
    'digits'               => ':attribute 必須為 :digits 位。',
    'digits_between'       => ':attribute 必須為 :min 與 :max 位之間。',
    'dimensions'           => ':attribute 圖像尺寸無效。',
    'distinct'             => ':attribute 欄有重複值。',
    'email'                => ':attribute 必須為一個有效的電子郵件地址。',
    'exists'               => '已選的 :attribute 無效。',
    'file'                 => ':attribute 必須為一個文件。',
    'filled'               => ':attribute 欄必須有一個值。',
    'gt'                   => [
        'numeric' => ':attribute 必須大於 :value 。',
        'file'    => ':attribute 必須多於 :value 千字節。',
        'string'  => ':attribute 必須多於 :value 字符。',
        'array'   => ':attribute 必須有超過 :value 項。',
    ],
    'gte'                  => [
        'numeric' => ':attribute 必須大於或等於 :value。',
        'file'    => ':attribute 必須大於或等於 :value 千字節。',
        'string'  => ':attribute 必須大於或等於 :value 字符。',
        'array'   => ':attribute 必須有不少於 :value 項。',
    ],
    'image'                => ':attribute 必須為一個圖像。',
    'in'                   => '已選的 :attribute 無效。',
    'in_array'             => ':attribute 欄在 :other 中不存在。',
    'integer'              => ':attribute 必須為整數。',
    'ip'                   => ':attribute 必須為一個有效的 IP 地址。',
    'ipv4'                 => ':attribute 必須為一個有效的 IPv4 地址。',
    'ipv6'                 => ':attribute 必須為一個有效的 IPv6 地址。',
    'json'                 => ':attribute 必須為一個有效的 JSON 字符串。',
    'lt'                   => [
        'numeric' => ':attribute 必須小於 :value。',
        'file'    => ':attribute 必須小於 :value 千字節。',
        'string'  => ':attribute 必須小於 :value 字符。',
        'array'   => ':attribute 必須有少於 :value 項。',
    ],
    'lte'                  => [
        'numeric' => ':attribute 必須小於或等於 :value。',
        'file'    => ':attribute 必須小於或等於 :value 千字節。',
        'string'  => ':attribute 必須小於或等於 :value 字符。',
        'array'   => ':attribute 必須不超過 :value 項。',
    ],
    'max'                  => [
        'numeric' => ':attribute 不許超過 :max。',
        'file'    => ':attribute 不許超過 :max 千字節。',
        'string'  => ':attribute 不許超過 :max 字符。',
        'array'   => ':attribute 不許有超過 :max 項。',
    ],
    'mimes'                => ':attribute 必須為一個類型： :values 的文件。',
    'mimetypes'            => ':attribute 必須為一個類型： :values 的文件。',
    'min'                  => [
        'numeric' => ':attribute 必須至少 :min。',
        'file'    => ':attribute 必須至少 :min 千字節。',
        'string'  => ':attribute 必須至少 :min 字符。',
        'array'   => ':attribute 必須有至少 :min 項。',
    ],
    'not_in'               => '已選的 :attribute 無效。',
    'not_regex'            => ':attribute 格式無效。',
    'numeric'              => ':attribute 必須為一個數字。',
    'present'              => ':attribute 欄必須存在。',
    'regex'                => ':attribute 格式無效。',
    'required'             => ':attribute 欄必須有。',
    'required_if'          => '當 :other 為 :value 時，:attribute 欄必須有。',
    'required_unless'      => ':attribute 欄必須有，除非 :other 位於 :values。',
    'required_with'        => '當 :values 存在時，:attribute 欄必須有。',
    'required_with_all'    => '當 :values 存在時，:attribute 欄必須有。',
    'required_without'     => '當 :values 不存在時，:attribute 欄必須有。',
    'required_without_all' => '當無 :values 存在時，:attribute 欄必須有。',
    'same'                 => ':attribute 與 :other 必須匹配。',
    'size'                 => [
        'numeric' => ':attribute 必須為 :size。',
        'file'    => ':attribute 必須為 :size 千字節。',
        'string'  => ':attribute 必須為 :size 字符。',
        'array'   => ':attribute 必須包含 :size 項。',
    ],
    'string'               => ':attribute 必須為一個字符串。',
    'timezone'             => ':attribute 必須為一個有效區。',
    'unique'               => ':attribute 已被採用。',
    'uploaded'             => ':attribute 上載失敗。',
    'url'                  => ':attribute 格式無效。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'callingCode' => [ // custom validation message for calling code not_regex
            'not_regex' => '電話區號欄必須有。'
        ],
        'country' => [
            'not_regex' => '國家必須有。',
            'required_calling_code' => '國家電話區號必須有。'
        ],
        'phone' => [ // custom validation message for phone review
            'not_found' => '電話號碼未找到。',
            'not_allowed' => '你只可在你訂購的時間內查看。'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
