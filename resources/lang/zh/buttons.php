<?php

return [

    'buy' => '現在購買',
    'sold_out' => '售磬',
    'submit' => '提交',
    'cancel' => '取消',
    'payment_sent' => '付款已發送',
    'show_qr' => '顯示 QR 碼',
    'continue_shopping' => '繼續購物',
    'login' => '登入',
    'adjust_stock' => '調整存貨',
    'delete' => '刪除',
    'sent' => '發送',
    'register' => '註冊',
    'review_add' => '添加評價'
];
