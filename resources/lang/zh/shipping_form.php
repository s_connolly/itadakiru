<?php

return [

    'title' => '發貨細節',
    'name' => '姓名',
    'country' => '國家',
    'city' => '城市',
    'address_1' => '地址行1',
    'address_2' => '地址行2',
    'postal' => '郵區',
    'phone' => '電話',
    'phone_tracking' => '包裹追踪資訊將通過短訊發送',
    'shipping_arrival' => '包裹運送時間可為1個星期',
    'shipping_included' => '包含郵費和進口稅',
    'shipping_countries' => '若想要發貨至其他國家，請聯繫我們'
];
