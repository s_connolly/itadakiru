<?php

return [
    'shippingCreated' => "你的 :productName 訂單已收到。稍後可提供追踪資訊。",
    'shippingSent' => ":productName 已發貨，應于一周內遞送至你手中。順豐速運 :trackingNumber。"
];
