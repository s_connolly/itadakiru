<?php

return [

    'btc_api_error' => '出現錯誤，不能設置比特幣價格。請檢查網絡連接並刷新頁面。',
    'btc_api_error_e1' => '出現錯誤。不能產生比特幣地址。請檢查網絡連接並重試。',
    'btc_api_error_e2' => '出現錯誤。不能產生比特幣地址。請檢查網絡連接並重試。',
    'btc_cypher_error' => '付款檢查出現錯誤，請檢查網絡連接並重試。請不要刷新頁面。',
    'created' => '已創建',
    'deleted' => '已更新',
    'keySaved' => '你的新密鑰已保存',
    'not_saved' => '數據未保存！',
    'notSaved' => '保存你的密鑰時出現錯誤',
    'shipmentSent' => '已發貨',
    'updated' => '已更新',
];
