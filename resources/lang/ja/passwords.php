<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'パスワードは最低でも6文字以上にする必要があります。また、パスワードは再確認する必要があります。',
    'reset' => 'パスワードが更新されました！',
    'sent' => 'パスワードリセットリンクをメールにて送信しました！',
    'token' => 'このパスワードリセットトークンは正しくありません。',
    'user' => "そのメールアドレスに紐づくユーザーを見つけることができませんでした。",

];
