<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '入力した情報がデータと一致しません',
    'throttle' => 'ログインに複数回失敗しました。:seconds秒後に再度お試し下さい',

];


  /*
   HELLO, ':seconds' do not translate
    */
