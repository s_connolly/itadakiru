<?php

return [

    'title' => '配送の詳細',
    'name' => '名前',
    'country' => '国',
    'city' => '都道府県',
    'address_1' => '住所1',
    'address_2' => '住所2',
    'postal' => '郵便番号',
    'phone' => '電話番号',
    'phone_tracking' => '追跡番号はSMSでお送りします',
    'shipping_arrival' => '到着まで1週間程度お待ち下さい',
    'shipping_included' => '配送料 & 関税込み',
    'shipping_countries' => 'その他の国への配送については、お問い合わせ下さい'
];