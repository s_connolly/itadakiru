<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attributeの同意は必須項目です。',
    'active_url'           => ':attributeは正しいURLではありません。',
    'after'                => ':attributeは、:date後である必要があります。',
    'after_or_equal'       => ':attributeは、:date後あるいは同じ日である必要があります。',
    'alpha'                => ':attributeには文字のみが含まれています。',
    'alpha_dash'           => ':attributeには、文字、数字、ハイフン、アンダーバーが含まれています。',
    'alpha_num'            => ':attributeには、文字と数字が含まれています。',
    'array'                => ':attributeは、配列である必要があります。',
    'before'               => ':attributeは、:dateよりも前の日にする必要があります。',
    'before_or_equal'      => ':attributeは、:dateよりも前の日か同じ日である必要があります。',
    'between'              => [
        'numeric' => ':attributeは、:minと:maxの間である必要があります。',
        'file'    => ':attributeは、最小:min、最大:maxキロバイトである必要があります。',
        'string'  => ':attributeは、:minから:max文字数にする必要があります。',
        'array'   => ':attributeは、:minから:max個である必要があります。',
    ],
    'boolean'              => ':attributeの項目は、正あるいは誤を入力して下さい。',
    'confirmed'            => ':attributeの確認が一致しません。',
    'date'                 => ':attributeの日にちが正しくありません。',
    'date_format'          => ':attributeがが:format表記と合致しません。',
    'different'            => ':attributeと:otherは、異なる必要があります。',
    'digits'               => ':attributeは、:digits桁である必要があります。',
    'digits_between'       => ':attributeは、最小:min、最大:max桁である必要があります。',
    'dimensions'           => ':attributeの画像サイズが適切ではありません。',
    'distinct'             => ':attributeに入力した内容が同じです。',
    'email'                => ':attributeに、正しいメールアドレスをご入力下さい。',
    'exists'               => '選択された:attributeは適切ではありません。',
    'file'                 => ':attributeはファイルである必要があります。',
    'filled'               => ':attributeに値を入力して下さい。',
    'gt'                   => [
        'numeric' => ':attributeは、:valueより大きい数字である必要があります。',
        'file'    => ':attributeは、:valueキロバイト以上である必要があります。',
        'string'  => ':attributeは、:value文字以上である必要があります。',
        'array'   => ':attributeは、:value個より多くなければなりません。',
    ],
    'gte'                  => [
        'numeric' => ':attributeは、:value以上か同じである必要があります。',
        'file'    => ':attributeは、:valueキロバイト以上か同じである必要があります。',
        'string'  => ':attributeは、:value文字以上か同じである必要があります。',
        'array'   => ':attributeは、:value個以上か同じである必要があります。',
    ],
    'image'                => ':attributeは、画像である必要があります。',
    'in'                   => '選択された:attributeは、正しくありません。',
    'in_array'             => ':attributeの項目は、:otherに存在しません。',
    'integer'              => ':attributeは整数である必要があります。',
    'ip'                   => ':attributeは、有効なIPアドレスである必要があります。',
    'ipv4'                 => ':attributeは、有効なIPv4アドレスである必要があります。',
    'ipv6'                 => ':attributeは、有効なIPv6アドレスである必要があります。',
    'json'                 => ':attributeは、有効なJSON文字列である必要があります。',
    'lt'                   => [
        'numeric' => ':attributeは、:value以下である必要があります。',
        'file'    => ':attributeは、:valueキロバイト以下である必要があります。',
        'string'  => ':attributeは、:value文字数以下である必要があります。',
        'array'   => ':attributeは、:value個以下である必要があります。',
    ],
    'lte'                  => [
        'numeric' => ':attributeは、:value以下あるいは同じである必要があります。',
        'file'    => ':attributeは、:valueキロバイト以下あるいは同じである必要があります。',
        'string'  => ':attributeは、:value文字数以下あるいは同じである必要があります。',
        'array'   => ':attributeは、:value個以上であってはいけません。',
    ],
    'max'                  => [
        'numeric' => ':attributeは、:maxより大きな値であってはいけません。',
        'file'    => ':attributeは、:maxキロバイトより大きくなってはいけません。',
        'string'  => ':attributeは、:max文字数より多くなってはいけません。',
        'array'   => ':attributeは、:max個より多くなってはいけません。',
    ],
    'mimes'                => ':attributeは、:valuesのファイルである必要があります。',
    'mimetypes'            => ':attributeは、:valuesのファイルである必要があります。',
    'min'                  => [
        'numeric' => ':attributeは、最低でも:minは必要です。',
        'file'    => ':attributeは、最低でも:minキロバイトは必要です。',
        'string'  => ':attributeは、最低でも:min文字数は必要です。',
        'array'   => ':attributeは、最低でも:min個は必要です。',
    ],
    'not_in'               => '選択された:attributeは、有効ではありません。',
    'not_regex'            => ':attributeのフォーマットが有効ではありません。',
    'numeric'              => ':attributeは、数字である必要があります。',
    'present'              => ':attributeは、現在値である必要があります。',
    'regex'                => ':attributeのフォーマットが有効ではありません。',
    'required'             => ':attributeの項目は入力必須です。',
    'required_if'          => ':attributeの項目は:otherが:valueであう場合は入力必須です。',
    'required_unless'      => ':attributeの項目は、:otherが:values内の場合は入力必須です。',
    'required_with'        => ':attributeの項目は、:valuesが現在値の場合は入力必須です。',
    'required_with_all'    => ':attributeの項目は、:valuesが現在値の場合は入力必須です。',
    'required_without'     => ':attributeの項目は、:valuesが現在値ではない場合は入力必須です。',
    'required_without_all' => ':attributeの項目は、:valuesがどれも現在値ではない場合は入力必須です。',
    'same'                 => ':attributeと:otherは一致する必要があります。',
    'size'                 => [
        'numeric' => ':attributeは、:sizeである必要があります。',
        'file'    => ':attributeは、:sizeキロバイトである必要があります。',
        'string'  => ':attributeは、:size文字数である必要があります。',
        'array'   => ':attributeは、:size個を含む必要があります。',
    ],
    'string'               => ':attributeは、文字列である必要があります。',
    'timezone'             => ':attributeは、有効なタイムゾーンである必要があります。',
    'unique'               => ':attributeは、既に取られています。',
    'uploaded'             => ':attributeのアップロードに失敗しました。',
    'url'                  => ':attributeのフォーマットが有効ではありません。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'カスタムメッセージ',
        ],
        'callingCode' => [ // custom validation message for calling code not_regex
            'not_regex' => '国番号の項目は入力必須です。'
        ],
        'country' => [
            'not_regex' => '国名の入力は必須です。',
            'required_calling_code' => '国番号の入力は必須です。'
        ],
        'phone' => [ // custom validation message for phone review
            'not_found' => '電話番号が見つかりませんでした。',
            'not_allowed' => 'レビューの数は、ご注文数と同じ数しかできません。'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
