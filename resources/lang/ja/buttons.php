<?php

return [

    'buy' => '購入する',
    'sold_out' => '売り切れ',
    'submit' => '送信',
    'cancel' => 'キャンセル',
    'payment_sent' => '支払いを送信しました',
    'show_qr' => 'QRコードを出す',
    'continue_shopping' => '買い物を続ける',
    'login' => 'ログイン',
    'adjust_stock' => '個数を調整する',
    'delete' => '削除',
    'sent' => '送信済み',
    'register' => '登録',
    'review_add' => 'レビューを追加する'
];
