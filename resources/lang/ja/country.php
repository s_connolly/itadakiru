
<?php

return [
    'usa' => 'アメリカ合衆国',
    'china' => '中国',
    'hongkong' => '香港',
    'japan' => '日本',
    'macau' => 'マカオ',
    'taiwan' => '台湾'
];
