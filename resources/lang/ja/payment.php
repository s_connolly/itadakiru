<?php

return [
    /* Payment */
    'payment_title' => '支払い情報',
    'item' => '商品',
    'amount' => '金額',

   'uses_bitcoin' => "ビットコインで支払います。ご注文の完了後、確定画面へリダイレクトされ、お使いの携帯電話へテキストメッセージが送信されます",

    'refresh' => "ページは更新しないで下さい",
    'seconds_to_confirm' => '確定に変わるまで約10秒程度かかります',
    'minutes_to_exchange' => '交換所からお支払いのビットコインは10-15分程度かかります',
    /* Owed */
    'owed' => 'ビットコインはまだ未払いです',
    'send_remaining' => "残りの金額を送信し、「支払いを送信済み」をクリックして下さい。",
    'send_again' => "エラーだと思う場合は、「支払いを送信済み」を再度クリックして下さい。",
    /* Confirmed */
    'confirmed_title' => 'ご注文ありがとうございます',
    'shipping_time' => '48時間以内に次の配送地へ発送します:'
];