<?php

return [
    'shippingCreated' => ":productNameのご注文を承りました。追跡番号が分かり次第お送りします",
   'shippingSent' => ":productNameが発送されました。1週間程度でお手元へお届けする予定です。SF エクスプレス :trackingNumber"
];
