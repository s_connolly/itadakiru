 <?php

return [
    'menu' => 'メニュー',
    'manage' => '管理',
    'pending' => '発送処理中',
    'sent' => '発送済み',
    'no_products' => '商品はありません',
    'no_pending' => '発送処理中の商品はありません',
    'no_sent' => '発送済みの商品はありません',
    'delete_confirm' => '本当に削除しても宜しいでしょうか'
    'gemini_key' => 'ジェミニキー'
];