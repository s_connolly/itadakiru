@extends('layouts.storefront')

@section('content')
    <div class="ui container grid mt-2">
        <div class="sixteen wide mobile twelve wide tablet twelve wide computer column centered">
            <h3 class="ui huge header center aligned">{{ __('faq.faq') }}</h3>
            <div class="ui styled fluid accordion">

                <!-- <div class="title active">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q1') }}
                </div>
                <div class="content active">
                    {{ __('faq.a1') }}
                </div> -->

                <div class="title active">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q2') }}
                </div>
                <div class="content">
                    {{ __('faq.a2') }}
                </div>
                <div class="title">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q3') }}
                </div>
                <div class="content">
                    {{ __('faq.a3') }}
                </div>
                <div class="title">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q4') }}
                </div>
                <div class="content">
                    {{ __('faq.a4') }}
                </div>
                <div class="title">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q5') }}
                </div>
                <div class="content">
                    {{ __('faq.a5') }}
                </div>
                <div class="title">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q6') }}
                </div>
                <div class="content">
                    {{ __('faq.a6') }}
                </div>
                <div class="title">
                    <i class="dropdown icon"></i>
                    {{ __('faq.q7') }}
                </div>
                <div class="content">
                    {{ __('faq.a7') }}
                </div>
            </div>
        </div>
    </div>
    <div class="ui hidden divider"></div>
@endsection
