@extends('layouts.storefront')

@section('page-style')
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
@endsection

@section('content')
    @if ($notification = session('notification'))
        <div class="ui container grid">
            <div class="column wide">
                {{ show_notification($notification['message'],$notification['type']) }}
            </div>
        </div>
    @endif
    <div class="ui grid container product-list">
        @foreach($product as $item)
            @php($rate = number_format( ($item->price) / $BTCprice, 6) )
            @php($hkd = $item->price * 7.8 )

            <div class="row">
                <div class="sixteen wide mobile eight wide tablet eight wide computer column middle aligned">
                    <div class="ui items">
                        <div class="item">
                            <div class="content" >
                                <div class="product-carousel">
                                    <div class="product-slide">
                                        <img class="ui centered medium image product__image" src="{{ asset('storage/'.$item->photo1) }}">
                                    </div>
                                    <div class="product-slide">
                                        <img class="ui centered medium image product__image" src="{{ asset('storage/'.$item->photo2) }}">
                                    </div>
                                    <div class="product-slide">
                                        <img class="ui centered medium image product__image" src="{{ asset('storage/'.$item->photo3) }}">
                                    </div>
                                    <div class="product-slide">
                                        <img class="ui centered medium image product__image" src="{{ asset('storage/'.$item->photo4) }}">
                                    </div>
                                    <div class="product-slide">
                                        <img class="ui centered medium image product__image" src="{{ asset('storage/'.$item->photo5) }}">
                                    </div>
                                    <div class="product-slide">
                                        <img class="ui centered medium image product__image" src="{{ asset('storage/'.$item->photo6) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sixteen wide mobile eight wide tablet six wide computer column middle aligned">
                    <div class="ui items">
                        <div class="item">
                            <div class="content">
                                <form method="GET" action="{{ action('ShippingFormController@shippingForm', ['product' => $item->id]) }}">
                                    @csrf
                                    <input type="hidden" name="rate" value={{ $rate }}>
                                    <h2 class="ui huge header aligned center text-capitalize">{{ $item->name }}</h2>
                                    <div class="description">
                                        @include('storefront.caseabout')
                                    </div>
                                    <div class="meta">
                                        <span class="mt-h">HKD {{$hkd}} / USD {{ $item->price }}</span>
                                        <span class="right floated">{{ $rate }} BTC</span>
                                    </div>
                                    @if($item->stock > 0)
                                        <button class="ui red button fluid">{{ __('buttons.buy') }}</button>
                                    @else
                                        <button type="button" class="ui disabled button fluid">{{ __('buttons.sold_out') }}</button>
                                    @endif

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="ui grid container">
        <div class="row">
            <div class="column">
                @include('partials.reviews',['reviews' => $reviews])
            </div>
        </div>
    </div>
@endsection
@section('page-script-before')
    <script src="{{ asset('js/slick.min.js') }}" ></script>
@endsection
@section('page-script-after')
    <script>
        $(document).ready(function(){
            $('.product-carousel').slick({
                lazyLoad: 'ondemand',
                dots: true,
                prevArrow:"<img class='a-left control-c prev slick-prev' src='{{ asset('images/arrow-left.png') }}'>",
                nextArrow:"<img class='a-right control-c next slick-next' src='{{ asset('images/arrow-right.png') }}'>"
            });
        });
    </script>
@endsection
