@extends('layouts.storefront')

@section('content')

    @if ($notification = session('notification'))
        <div class="ui container grid">
            <div class="column wide">
                {{ show_notification($notification['message'],$notification['type']) }}
            </div>
        </div>
    @endif
    <div class="ui grid container">
        <div class="row pt-2 pb-4">
            <div class="sixteen wide mobile eight wide tablet eight wide computer column">
                <div class="ui items">
                    <div class="item mt-2">
                        <div class="content text-center">
                            <h2 class="ui huge header text-capitalize mb-h">{{ $product->name }}</h2>
                            <img class="ui medium centered image" src="{{ asset('storage/'.$product->photo1) }}">
                            <h3 class="ui small header mt-h">{{ $rate }}BTC</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sixteen wide mobile eight wide tablet eight wide computer column middle aligned">
                <div class="ui items">
                    <div class="item">
                        <div class="content">
                            <form class="ui form"  method="GET" action="{{ action('ShippingFormController@validateShippingForm', ['product' => $product->id ]) }}">
                                @csrf
                                <h3 class="ui header center aligned text-uppercase">{{ __('shipping_form.title') }}</h3>
                                <div class="field {{ ($errors->first('name')) ? 'error' : '' }}">
                                    <input type="hidden" name="rate" value={{ $rate }}>
                                    <label for="name">{{ __('labels.name') }}</label>
                                    <input type="text" id="name" name="name" value="{{old('name')}}">
                                </div>
                                <div class="field">
                                    <div class="two fields">
                                        <div class="eight wide field {{ ($errors->first('country')) ? 'error' : '' }}">
                                            <label for="country">{{ __('labels.country') }}</label>
                                            <div id="countryDropdown" class="ui fluid selection dropdown">
                                                <input id="country" type="hidden" name="country" value="{{ old('country','0|'.set_country_name($country)) }}">
                                                <i class="dropdown icon"></i>
                                                <div class="default text">{{ __('labels.country') }}</div>
                                                <div class="menu">
                                                    <!-- <div class="item" data-value="0|USA">{{ __('country.usa') }}</div> -->
                                                    <div class="item" data-value="0|Japan">{{ __('country.japan') }}</div>
                                                    <div class="item" data-value="0|Hong Kong">{{ __('country.hongkong') }}</div>
                                                    <div class="item" data-value="0|Taiwan">{{ __('country.taiwan') }}</div>
                                                    <div class="item" data-value="0|Macau">{{ __('country.macau') }}</div>
                                                    <div class="item" data-value="0|China">{{ __('country.china') }}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="eight wide field {{ ($errors->first('state')) ? 'error' : '' }}">
                                            <label for="state">{{ __('labels.state') }}</label>
                                            <input type="text" id="state" class="input" name="state" value="{{ old('state') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="two fields">
                                        <div class="eight wide field {{ ($errors->first('city')) ? 'error' : '' }}">
                                            <label for="city">{{ __('labels.city') }}</label>
                                            <input type="text" id="city" class="input" name="city" value="{{ old('city') }}">
                                        </div>
                                        <div class="eight wide field {{ ($errors->first('postal')) ? 'error' : '' }}">
                                            <label for="postal">{{ __('labels.postal') }}</label>
                                            <input type="text" id="postal" class="input" name="postal" value="{{ old('postal',set_postal_code($country)) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="field {{ ($errors->first('address1')) ? 'error' : '' }}">
                                    <label for="address1">{{ __('labels.address_1') }}</label>
                                    <input type="text" id="address1" class="input" name="address1" value="{{ old('address1') }}">
                                </div>
                                <div class="field">
                                    <label for="address2">{{ __('labels.address_2') }}</label>
                                    <input type="text" id="address2" class="input" name="address2" value="{{ old('address2') }}">
                                </div>
                                <div class="field {{ ($errors->first('phone')) ? 'error' : '' }}">
                                    <label for="phone">{{ __('labels.phone') }} <small>({{__('labels.phone_tracking')}})</small></label>
                                    <div class="two fields">
                                        <div class="five wide field {{ ($errors->first('callingCode')) ? 'error' : '' }}">
                                            <div id="callingCodeDropdown" class="ui fluid selection dropdown">
                                                <input id="callingCode" type="hidden" name="callingCode" value="{{ old('callingCode',set_calling_code($country)) }}">
                                                <i class="dropdown icon"></i>
                                                <div class="default text">{{ __('labels.callingCode') }}</div>
                                                <div class="menu">
                                                   <!--  <div class="item" data-value="+1|USA"><i class="us flag"></i>+1</div> -->
                                                    <div class="item" data-value="+81|Japan"><i class="jp flag"></i>+81</div>
                                                    <div class="item" data-value="+852|Hong Kong"><i class="hk flag"></i>+852</div>
                                                    <div class="item" data-value="+886|Taiwan"><i class="tw flag"></i>+886</div>
                                                    <div class="item" data-value="+853|Macau"><i class="mo flag"></i>+853</div>
                                                    <div class="item" data-value="+86|China"><i class="cn flag"></i>+86</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="eleven wide field {{ ($errors->first('phone')) ? 'error' : '' }}">
                                            <input type="text"  class="input" name="phone" value="{{ old('phone') }}" @keypress="isNumber($event)" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false">
                                        </div>
                                    </div>
                                </div>

                                <div class="field">
                                    <div class="two fields">
                                        <div class="field">
                                            <a href="{{ URL::to('/') }}" class="ui button fluid">{{ __('buttons.cancel') }}</a>
                                        </div>
                                        <div class="field">
                                            <button class="ui red button fluid" type="submit">{{ __('buttons.submit') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="text-center">
                                <p>{{ __('shipping_form.shipping_arrival') }}</p>
                                <p>{{ __('shipping_form.shipping_included') }}</p>
                                <p>{{ __('shipping_form.shipping_countries') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('page-script-after')
    <script>
        $(document).ready(function(){

            $('.ui.form').form({
                fields: {
                    name     : 'empty',
                    country   : 'not[0|Country]',
                    state : 'empty',
                    city : 'empty',
                    state : 'empty',
                    address1: 'empty',
                    postal: 'empty',
                    callingCode: 'not[0|Country]',
                    phone: ['empty','integer']
                }
            });

            $('#countryDropdown').dropdown({
                onChange: function() {

                    var country = ($('#country').val()).split('|');
                    //set calling code
                    $('#callingCodeDropdown').dropdown('set selected',get_calling_code(country[1]));
                    //trigger change event
                    $('#country').change();

                    if (country[1] === 'Hong Kong' || country[1] === 'Macau') {

                        $('#postal').val('000000');
                    } else {
                        $('#postal').val('');

                       // ($('#postal').parent('.field')).addClass('error');
                    }

                    remove_error();
                }
            });
        });

        /* Returns calling code value */
        function get_calling_code(country) {

            var callingCodes = {!! json_encode(config('dropdowns.callingCode')) !!}

            return callingCodes[country]+'|'+country;
        }

        /* Removes error class as semantic does not detect change on error */
        function remove_error() {

            var postal = $('#postal'),
                postalParent = postal.parent('.field'),
                callingCodeParent = $('#callingCodeDropdown').parent('.field');

            if(postalParent.hasClass('error') && (postal.val()).trim() != '') {

                postalParent.removeClass('error');
            }

            if(callingCodeParent.hasClass('error')) {

                callingCodeParent.removeClass('error');
            }
        }
    </script>
@endsection
