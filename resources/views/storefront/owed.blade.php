@extends('layouts.storefront')

@section('content')
    <div class="ui container grid payment-wrap">
        <div class="column middle aligned">
            <div class="ui two stackable cards mt-1">
                <div class="card">
                    <div class="content">
                        <div class="center aligned header">
                            <h3 class="ui large header">{{ __('payment.payment_title') }}</h3>
                        </div>
                        <table class="ui very basic unstackable table">
                            <tbody>
                                <tr><td colspan="2"></td></tr>
                                <tr>
                                    <td><strong>{{ __('payment.item') }}</strong></td>
                                    <td>{{$product->name}}</td>
                                </tr>
                                <tr>
                                    <td><strong>{{ __('payment.amount') }}</strong></td>
                                    <td>{{$btcinfo[2]}} BTC</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="center aligned">
                            <button id="qr-modal-button" class="mini ui green button">{{ __('buttons.show_qr') }}</button>
                        </div>
                        <div class="center aligned mt-1">
                            <a id="btc-address" class="ui header color--blue break-word" href="https://live.blockcypher.com/btc/address/{{$btcinfo[1]}}" target="_blank">{{$btcinfo[1]}}</a>
                        </div>
                        <form action="{{ action('PaymentController@check',['product' => $product->id])}}" id="payment-sent-form" class="mt-1">
                            @csrf
                            <input type="hidden" name="rate" value="{{$btcinfo[2]}}">
                            <input type="hidden" name="address" value="{{$btcinfo[1]}}">
                            <button class="ui red button fluid">{{ __('buttons.payment_sent') }}</button>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="content">
                        <div class="center aligned">
                            <svg width="101" viewBox="0 0 101 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <a href="" class="svg-fallback">
                                <path d="M45 65H55V75H45V65ZM45 25H55V55H45V25ZM49.95 0C22.35 0 0 22.4 0 50C0 77.6 22.35 100 49.95 100C77.6 100 100 77.6 100 50C100 22.4 77.6 0 49.95 0ZM50 90C27.9 90 10 72.1 10 50C10 27.9 27.9 10 50 10C72.1 10 90 27.9 90 50C90 72.1 72.1 90 50 90Z" fill="#F12828"/>
                                </a>
                                <image src="{{ asset('images/circular-warning.png') }}" xlink:href="">
                            </svg>
                            <h4 class="ui red large header">{{$btcinfo[0]}} {{ __('payment.owed') }}</h4>
                            <p>{{ __('payment.send_remaining') }}</p>
                            <p>{{ __('payment.send_again') }} </p>
                            <p>{{ __('payment.minutes_to_exchange') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ui hidden divider"></div>
    @include('layouts.modals.qrModal')
@endsection
@section('page-script-before')
    <script src="{{ asset('js/qrcode.min.js') }}"></script>
    <script src="{{ asset('js/qrgenerate.js') }}"></script>
@endsection
