@extends('layouts.storefront')

@section('content')
    <div class="ui container grid payment-wrap">
        <div class="sixteen wide mobile eight wide tablet eight wide computer column centered middle aligned">
            <div class="ui card fluid mt-1">
                <div class="content">
                    <div class="center aligned header"><h3 class="ui header large">{{ __('payment.confirmed_title') }}</h3></div>
                    <div class="center aligned mt-1">
                        <svg width="101" viewBox="0 0 101 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <a href="" class="svg-fallback">
                                <path d="M73.7498 27.9L40.7998 60.85L22.8498 42.95L15.7998 50L40.7998 75L80.7998 35L73.7498 27.9ZM50.7998 0C23.1998 0 0.799805 22.4 0.799805 50C0.799805 77.6 23.1998 100 50.7998 100C78.3998 100 100.8 77.6 100.8 50C100.8 22.4 78.3998 0 50.7998 0ZM50.7998 90C28.6998 90 10.7998 72.1 10.7998 50C10.7998 27.9 28.6998 10 50.7998 10C72.8998 10 90.7998 27.9 90.7998 50C90.7998 72.1 72.8998 90 50.7998 90Z" fill="#56CCF2"/>
                            </a>
                            <image src="{{ asset('images/circular-check.png') }}" xlink:href="">
                        </svg>
                    </div>
                    <div class="center aligned description mt-1">
                        <p> <strong> {{ $confirmed['product'] }} </strong> {{ __('payment.shipping_time') }} <strong> {{ $confirmed['city'] }} </strong></p> 
                    </div>
                    <a href="{{ URL::to('/') }}"class="ui red button fluid mt-1">{{ __('buttons.continue_shopping') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="ui hidden divider"></div>
@endsection
