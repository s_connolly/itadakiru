@extends('layouts.admin')

@section('content')
    <div class="ui card fluid">
        <div class="content">
            <div class="header">
                <h2 class="ui header">{{ __('admin.pending') }}</h2>
            </div>
        </div>
        <div class="content">
            <table class="ui celled table">
                <thead>
                    <th>{{ __('labels.product') }}</th>
                    <th>{{ __('labels.name') }}</th>
                    <th>{{ __('labels.country') }}</th>
                    <th>{{ __('labels.state') }}</th>
                    <th>{{ __('labels.city') }}</th>
                    <th>{{ __('labels.postal') }}</th>
                    <th>{{ __('labels.address_1') }}</th>
                    <th>{{ __('labels.address_2') }}</th>
                    <th>{{ __('labels.phone') }}</th>
                    <th class="right aligned">{{ __('labels.options') }}</th>
                </thead>
                <tbody>
                    @if($pending->count())
                        @foreach($pending as $shipment)
                            <tr>
                                <td>{{ $shipment->product_name }}</td>
                                <td>{{ $shipment->name }}</td>
                                <td>{{ $shipment->country }}</td>
                                <td>{{ $shipment->state }}</td>
                                <td>{{ $shipment->city }}</td>
                                <td>{{ $shipment->postal }}</td>
                                <td>{{ $shipment->address1 }}</td>
                                <td>{{ $shipment->address2 }}</td>
                                <td>{{ $shipment->phone }}</td>
                                <td>
                                    <div class="ui items">
                                        <div class="item">
                                            <div class="content">
                                            <form method="GET" action="{{ action('AdminController@delete') }}" class="ui right floated" onSubmit="return confirm('{{ __('admin.delete_confirm') }} {{ $shipment->name }}');">
                                                <input type="hidden" name="delete" value="{{ $shipment->id }}" />
                                                <button class="mini ui red button">{{ __('buttons.delete') }}</button>
                                            </form>
                                            <form method="GET" action="{{ action('AdminController@sent',['id' => $shipment->id]) }}" id="sentForm-{{ $shipment->id }}" class="ui right floated">
                                                <input type="hidden" class="trackingNumber" name="trackingNumber" value="0">
                                                <button class="mini ui teal button sent-button" type="button">{{ __('buttons.sent') }}</button>
                                            </form>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="text-center">{{ __('admin.no_pending') }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @include('layouts.modals.trackingNumberModal')
@endsection
@section('page-script-after')
    <script>
        $(document).ready(function(){
            //shows modal
            $('.sent-button').click(function(){

                //get parent form
                var parent = $(this).parent('form');

                //set modal hidden field to get form id
                $('#shipmentFormId').val(parent.attr('id'));

                //trigger modal
                $('#tracking-number-modal').modal('show');
            });

            //set validation rules
            $('.ui.form')
                .form({
                    fields: {
                        trackingNumberInput: 'empty'
                    }
                });

            //checks the tracking number and submit form
            $('#trackingNumberButton').click(function(){

                var recordForm = $('#'+$('#shipmentFormId').val()),
                    trackingNumber = recordForm.find('.trackingNumber');

                if( $('.ui.form').form('is valid', 'trackingNumberInput')) {

                    trackingNumber.val($('#trackingNumberInput').val());

                    recordForm.submit();
                }
            });
        });
    </script>
@endsection
