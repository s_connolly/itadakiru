@extends('layouts.admin')

@section('content')
    <div class="ui card fluid">
        <div class="content">
            <div class="header">
                <h2 class="ui header">{{ __('admin.gemini_key') }}</h2>
            </div>
        </div>
        <div class="content">
            <form  method="POST" action="{{ action('KeysController@store') }}" class="ui form">
                @csrf
                <div class="two fields">
                    <div class="field">
                        <label for="key">{{ __('labels.key') }}</label>
                        <input id="key" type="text" class="input" name="key" value="{{ old('key') }}">
                    </div>
                    <div class="field">
                        <label for="secret">{{ __('labels.secret') }}</label>
                        <input id="secret" type="text" class="input" name="secret" value="{{ old('secret') }}">
                    </div>
                </div>
                <button class="ui red button right floated" type="submit">{{ __('buttons.submit') }}</button>
            </form>
        </div>
        @include('layouts.errors')
        
    </div>
@endsection
@section('page-script-after')
    <script>
        $(document).ready(function(){
            $('.ui.form').form({
                fields: {
                    key : 'empty',
                    secret : 'empty'
                }
            });
        
        });
    </script>
@endsection
