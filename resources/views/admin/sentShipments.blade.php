@extends('layouts.admin')

@section('content')
    <div class="ui card fluid">
        <div class="content">
            <div class="header">
                <h2 class="ui header">{{ __('admin.sent') }}</h2>
            </div>
        </div>
        <div class="content">
            <table class="ui celled table">
                <thead>
                    <th>{{ __('labels.product') }}</th>
                    <th>{{ __('labels.name') }}</th>
                    <th>{{ __('labels.country') }}</th>
                    <th>{{ __('labels.state') }}</th>
                    <th>{{ __('labels.city') }}</th>
                    <th>{{ __('labels.postal') }}</th>
                    <th>{{ __('labels.address_1') }}</th>
                    <th>{{ __('labels.address_2') }}</th>
                    <th>{{ __('labels.phone') }}</th>
                    <th class="right aligned">{{ __('labels.options') }}</th>
                </thead>
                <tbody>
                    @if($sent)
                        @foreach($sent as $shipment)
                            <tr>
                                <td>{{ $shipment->product_name }}</td>
                                <td>{{ $shipment->name }}</td>
                                <td>{{ $shipment->country }}</td>
                                <td>{{ $shipment->state }}</td>
                                <td>{{ $shipment->city }}</td>
                                <td>{{ $shipment->postal }}</td>
                                <td>{{ $shipment->address1 }}</td>
                                <td>{{ $shipment->address2 }}</td>
                                <td>{{ $shipment->phone }}</td>
                                <td>
                                    <form method="GET" action="{{ action('AdminController@delete') }}" class="ui right floated" onSubmit="return confirm('{{ __('admin.delete_confirm') }} {{ $shipment->name }}');">
                                        <input type="hidden" name="delete" value="{{ $shipment->id }}" />
                                        <button class="mini ui red button">{{ __('buttons.delete') }}</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9" class="text-center">{{ __('admin.no_sent') }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
