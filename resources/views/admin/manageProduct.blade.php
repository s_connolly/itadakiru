@extends('layouts.admin')

@section('content')
    <div class="ui card fluid">
        <div class="content">
            <div class="header">
                <h2 class="ui header">{{ __('admin.manage') }}</h2>
            </div>
        </div>
        <div class="content">
            <form  method="POST" action="{{ action('AdminController@create') }}" enctype="multipart/form-data" class="ui form">
                @csrf
                <div class="three fields">
                    <div class="field">
                        <label for="name">{{ __('labels.name') }}</label>
                        <input type="text" id="name" class="input" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="field">
                        <label for="code">{{ __('labels.code') }}</label>
                        <input type="text" id="code" class="input" name="code" value="{{ old('code') }}">
                    </div>
                    <div class="field">
                        <label for="price">{{ __('labels.price') }}</label>
                        <input type="text" id="price" class="input" name="price" value="{{ old('price') }}">
                    </div>
                </div>
                <div class="three fields">
                    <div class="field">
                        <label for="photo1">{{ __('labels.photo') }} 1 <small class="text-muted">(jpeg, png, max 5MB)</small></label>
                        <input type="file" id="photo1" name="photo1">
                    </div>
                    <div class="field">
                        <label for="photo2">{{ __('labels.photo') }} 2 <small class="text-muted">(jpeg, png, max 5MB)</small></label>
                        <input type="file" id="photo2" name="photo2" >
                    </div>
                    <div class="field">
                        <label for="photo3">{{ __('labels.photo') }} 3 <small class="text-muted">(jpeg, png, max 5MB)</small></label>
                        <input type="file" id="photo3" name="photo3" >
                    </div>
                </div>
                <div class="three fields">
                    <div class="field">
                        <label for="photo4">{{ __('labels.photo') }} 4 <small class="text-muted">(jpeg, png, max 5MB)</small></label>
                        <input type="file" id="photo4" name="photo4" >
                    </div>
                    <div class="field">
                        <label for="photo5">{{ __('labels.photo') }} 5 <small class="text-muted">(jpeg, png, max 5MB)</small></label>
                        <input type="file" id="photo5" name="photo5" >
                    </div>
                    <div class="field">
                        <label for="photo6">{{ __('labels.photo') }} 6 <small class="text-muted">(jpeg, png, max 5MB)</small></label>
                        <input type="file" id="photo6" name="photo6" >
                    </div>
                </div>
                <button class="ui red button right floated" type="submit">{{ __('buttons.submit') }}</button>
            </form>
        </div>
        @include('layouts.errors')
        
    </div>
    <div class="ui card fluid">

        <div class="content">
            <table class="ui celled table">
                <thead>
                    <th>{{ __('labels.name') }}</th>
                    <th>{{ __('labels.price') }}</th>
                    <th>{{ __('labels.stock') }}</th>
                    <th>{{ __('labels.orders') }}</th>
                    <th class="right aligned">{{ __('labels.options') }}</th>
                </thead>
                <tbody>
                    @if($products->count())
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->stock }}</td>
                                <td>{{ $product->orders }}</td>
                                <td class="right aligned">
                                    <div class="ui items">
                                        <div class="item">
                                            <div class="content">
                                                <form method="GET" action="{{ action('AdminController@productDelete',['id' => $product->id ]) }}" class="ui right floated" onSubmit="return confirm('{{ __('admin.delete_confirm') }} {{ $product->name }}?');">
                                                    <button class="mini ui red button">{{ __('buttons.delete') }}</button>
                                                </form>
                                                <stock-adjust 
                                                    class="ui right floated"
                                                    :quantity="{{ $product->stock }}"
                                                    :action="'{{ action('AdminController@stock') }}'"
                                                    :product-id="{{ $product->id }}"
                                                    :text="'{{ __('buttons.adjust_stock') }}'">
                                                </stock-adjust>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="ui center aligned">
                            <td colspan="5" class="text-center"> {{ __('admin.no_products') }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('page-script-after')
    <script>
        $(document).ready(function(){
            $('.ui.form').form({
                fields: {
                    name : 'empty',
                    code : 'empty',
                    price : 'empty',
                    photo1 : 'empty',
                    photo2 : 'empty',
                    photo3 : 'empty',
                    photo4 : 'empty',
                    photo5 : 'empty',
                    photo6 : 'empty'
                }
            });
        });
    </script>
@endsection
