<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Itadakiru</title>
    <meta name="description">

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="ht-100 bg--background-grey">
        <div class="ui grid container ht-100 middle aligned">
            <div class="sixteen wide mobile eight wide tablet seven wide computer column centered">
                <div class="ui card fluid">
                    <div class="content">
                        <div class="header">Itadakiru Login</div>
                    </div>
                    <div class="content">
                        <form method="POST" action="{{ action('Auth\loginController@login') }}" class="ui form">
                            @csrf
                            <div class="field">
                                <label for="username">{{ __('labels.username') }}</label>
                                <input type="text" id="username" name="username" value="{{ old('username') }}" placeholder="Username" required>
                                @if ($errors->has('username'))
                                    <div class="ui pointing red basic label">
                                        {{ $errors->first('username') }}
                                    </div>
                                @endif
                            </div>
                            <div class="field">
                                <label for="password">{{ __('labels.password') }}</label>
                                <input type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <span class="error-text"> {{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <button class="ui red button fluid" type="submit">{{ __('buttons.login') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery.3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/semantic.min.js') }}"></script>
</body>
</html>
