<div class="ui grid">
    <div class="sixteen wide mobile twelve wide tablet eight wide computer column centered" v-if="reviews.data != 0">
        <div class="ui comments">
            <div class="comment" v-for="(review,key) in reviews.data" :key="key">
                <div class="content">
                    <div class="metadata m-0">
                        <span class="date">@{{ formatDate(review.created_at) }}</span>
                    </div>
                    <div class="text">
                        @{{ review.comment }}
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <pagination :data="reviews" v-bind:show-disabled="true" icon="chevron" v-on:change-page="getReviews"></pagination>
        </div>
    </div>
</div>
