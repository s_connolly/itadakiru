<div class="ui accordion">                   
    <div class="title">
        <div class="ui horizontal divider">
            {{ __('headings.reviews') }} <i class="dropdown icon"></i>
        </div>
    </div>
    <div class="content no-js">
        @include('partials.review-list',['reviews'=>$reviews])
        <div class="ui grid">
            <div class="sixteen wide mobile twelve wide tablet eight wide computer column centered">
                @include('partials.review-form')
            </div>
        </div>
    </div>
</div>
<div class="ui hidden divider"></div>
