<form  method="POST" action="{{ action('ReviewController@store') }}" class="ui form form-submit" @submit.prevent="saveReview">
    @csrf
    <h4 class="ui large header center aligned text-uppercase mt-0">{{ __('headings.review_add') }}</h4>
    <div class="field">
        <label for="phone">{{ __('labels.phone') }} <small>({{__('labels.phone_privacy')}})</small></label>
        <div class="two fields">
            <div class="four wide field {{ ($errors->first('callingCode')) ? 'error' : '' }}">
                <div class="ui fluid selection dropdown no-js--hidden" :class="{'error': reviewErrors.callingCode.length }">
                    <input type="hidden" name="callingCode" value="{{ old('callingCode','0|Code') }}" ref="callingCode">
                    <i class="dropdown icon"></i>
                    <div class="default text">{{ __('labels.callingCode') }}</div>
                    <div class="menu">
                        <!--<div class="item" data-value="+1|USA"><i class="us flag"></i>+1</div>-->
                        <div class="item" data-value="+81|Japan"><i class="jp flag"></i>+81</div>
                        <div class="item" data-value="+852|Hong Kong"><i class="hk flag"></i>+852</div>
                        <div class="item" data-value="+886|Taiwan"><i class="tw flag"></i>+886</div>
                        <div class="item" data-value="+853|Macau"><i class="mo flag"></i>+853</div>
                        <div class="item" data-value="+86|China"><i class="cn flag"></i>+86</div>
                    </div>
                </div>
            </div>
            <div class="twelve wide field {{ ($errors->first('phone')) ? 'error' : '' }}" :class="{'error': reviewErrors.phone.length }">
                <input type="text" id="phone" class="input" name="phone" value="{{ old('phone') }}" v-model="review.phone">

            </div>
        </div>
    </div>
    <div class="field {{ ($errors->first('comment')) ? 'error' : '' }}" :class="{'error': reviewErrors.comment.length}">
        <label for="comment">{{ __('labels.comment') }}</label>
        <textarea id="comment"  name="comment" v-model="review.comment">{{ old('comment','') }}</textarea>
    </div>
    <button class="ui red button fluid" type="submit" :class="{'loading':loading,'disabled':loading}">{{ __('buttons.submit') }}</button>
</form>
<div  v-show="reviewErrors.phone.length || reviewErrors.comment.length || reviewErrors.callingCode.length" class="ui error message text-center">
    <div class="items">
        <div class="item"  v-for="(error,key) in reviewErrors" :key="key">@{{ reviewErrors[key][0] }}</div>
    </div>
</div>
