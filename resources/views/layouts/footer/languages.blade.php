<div class="ui stackable three column padded grid">
    <div class="row pb-0">
        <div class="column footer__left">
            <div class="ui horizontal list">
                <div class="item">
                    <a href="{{ action('LocaleController@setLocale',['locale' => 'en']) }}" class="{{ set_active_locale('en') }}">English</a>
                </div>
                <div class="item">
                    <a href="{{ action('LocaleController@setLocale',['locale' => 'ja']) }}" class="{{ set_active_locale('ja') }}">日本語</a>
                </div>
                <div class="item">
                    <a href="{{ action('LocaleController@setLocale',['locale' => 'zh']) }}" class="{{ set_active_locale('zh') }}">普通話</a>
                </div>
            </div>
        </div>
        <div class="column footer__center">Service@ItadakiruDolls.com</div>
        <div class="column footer__right"><a href="{{ action('IndexController@faq') }}" target="_blank">{{ __('footer.faq') }}</a></div>
    </div>
</div>
