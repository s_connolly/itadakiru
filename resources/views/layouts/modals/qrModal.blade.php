<div id="qr-modal" class="ui mini modal">
    <i class="close icon"></i>
    <div class="content">
        <h5 class="ui large header center aligned"> {{ __('modal.btc_address') }} </h5>
        <div id="qrcode"></div>
    </div>
</div>
