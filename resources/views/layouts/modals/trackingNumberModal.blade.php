<div id="tracking-number-modal" class="ui mini modal">
    <i class="close icon"></i>
    <div class="content">
        <h5 class="ui large header center aligned"> Tracking Number </h5>
        <form class="ui form">
            <div class="ui fluid action input" data-children-count="1">
                <input type="text" id="trackingNumberInput" placeholder="Tracking Number">
                <input type="hidden" id="shipmentFormId" value="0">
                <div id="trackingNumberButton" class="ui teal button">Sent</div>
            </div>
        </form>
    </div>
</div>
