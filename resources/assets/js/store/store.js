import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

let store = new Vuex.Store({
    state: {
        reviews: {}
    },
    getters: {

    },
    mutations: {
        updateReviews: function(state,reviews) {
            state.reviews = reviews;
        }
    },
    actions: {

    }
});

export default store;