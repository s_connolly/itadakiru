
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';

import store from './store/store';

import PageScripts from './page-scripts';

import Moment from 'moment'; //review date Time


/* Global vue components */
Vue.component('stock-adjust', require('./components/admin/StockAdjust.vue'));

Vue.component('pagination', require('laravel-vue-semantic-ui-pagination'));

/* Initialize vue */
const app = new Vue({
    el: '#app',
    store,
    data: {
        reviews: {},
        review: {
            callingCode: '0',
            phone: '',
            comment: ''
        },
        loading: false,
        reviewErrors:{
            phone:[],
            comment:[],
            callingCode:[]

        }
    },
    methods: {

        /* Get the paginated results */
        getReviews: function(page = 1) {

            axios.get(this.$refs.baseUrl.value+'/reviews/getReviews?page=' + page)
				.then(response => {
					this.reviews = response.data;
			});
        },
        /* Saves the review */
        saveReview: function() {

            let vm = this;

            vm.loading = true;

            if (this.loading) {

                axios.post(this.$refs.baseUrl.value+'/store',{
                    callingCode: this.$refs.callingCode.value,
                    phone: this.review.phone,
                    comment: this.review.comment
                }).then(function(response){

                    if(response.status === 200) {
                        window.location = vm.$refs.baseUrl.value;
                    }
                }).catch(function(error){

                    if (error.response) {

                        let errors = error.response.data.errors;

                        vm.reviewErrors = {
                            phone: [],
                            callingCode: [],
                            comment: []
                        };

                        Object.keys(errors).forEach(function(key,index) {

                            vm.reviewErrors[key] = errors[key];
                        });

                        vm.loading = false;

                    }

                });
            }

        },
        /* Format the created at date */
        formatDate: function(date) {

            if(document.documentElement.lang === 'ja') {
                Moment.locale('ja');
                return Moment(date).format("MMM Do YYYY"+'年');
            }

            if(document.documentElement.lang === 'zh') {

                Moment.locale('zh-cn');

                return Moment(date).format("MMM Do YYYY"+'年');
            }

            return Moment(date).format("MMM DD YYYY");
        },
        // Number only input
        isNumber: function(evt) {

            evt = (evt) ? evt : window.event;
            
            let  charCode = (evt.which) ? evt.which : evt.keyCode;

            if ((charCode > 31 && (charCode < 48 || charCode > 57)) || charCode === 46) {
                evt.preventDefault();;
            } else {
                return true;
            }
        }
    },
    mounted() {

        this.getReviews();

        PageScripts.init();

    }
});
