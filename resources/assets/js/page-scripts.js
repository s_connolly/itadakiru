/* Global page scipts */
const PageScripts = (function(){

    function init() {

        // Semantic close button
        $('.message .close').on('click', function() {
            $(this).closest('.message').transition('fade');
        });
        
        // Semantic accordion
        $('.ui.accordion').accordion();

        // Semantic dropdown
        $('.ui.dropdown').dropdown();

        // Payment Sent Modal
        $('#payment-sent-form').on('submit', function(){
           // $('#payment-sent-modal').modal('show');
           $('#dimmer').dimmer('show');
        });

        // Qr Modal
        $('#qr-modal-button').click(function(){
            $('#qr-modal').modal('show');
        });

        // Removes noscript tags when js enabled
       // $('.with-js').parents('noscript').remove();
    }

    return {
        init
    }
})();

export default PageScripts;