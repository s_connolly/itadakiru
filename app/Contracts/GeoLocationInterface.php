<?php

namespace App\Contracts;

interface GeoLocationInterface 
{
    public function getCountry();
}