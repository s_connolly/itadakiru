<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\products;
use App\payments;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use App\Notifications\ShipmentUpdate;
use App\Services\GeoPlugin;
use App\Services\GeoLocation;

class shipments extends Model
{
  use Notifiable;

  public function createShipment(Request $request, product $product) {

    $shipping = session('shipping');

    $phone = $shipping['callingCode'] . ' ' . $shipping['phone'];

    $shipment = shipments::create([
      'product_id' => $product->id,
      'product_name' => $product->name,
      'name' => $shipping['name'],
      'country' => $shipping['country'],
      'city' => $shipping['city'],
      'postal' => $shipping['postal'],
      'address1' => $shipping['address1'],
      'address2' => $shipping['address2'],
      'phone' => $phone,
      'sent' => 0,
      'date' => null,
      'state' => $shipping['state']
    ]);

    $new = new product;
    $new->minus_stock($product->id); //Add a queqe to prevent over orders
    $new->ordered($product->id); //Add a queqe to prevent over orders
    
    try {
      $shipment->notify(new ShipmentUpdate('created',$shipment));
    } catch (\Exception $e) {
      Log::error($e->getMessage());
    }
  }

  public function sent($id) {

    $product = shipments::find($id);
    $product->sent = 1;
    $product->date = Carbon::now();
    $product->save();

    $trackingNumber = request('trackingNumber');

    try {
      $product->notify(new ShipmentUpdate('sent',$product,$trackingNumber));
    } catch (\Exception $e) {
      Log::error($e->getMessage());
    }


  }

  /**
   * Route notifications for the Nexmo channel.
   *
   * @param  \Illuminate\Notifications\Notification  $notification
   * @return string
   */
  public function routeNotificationForNexmo($notification)
  {
    return $this->formatPhoneNumber($this->phone);
  }

     /**
   * Removes + and space in phone number
   *
   * @param $phoneNumber
   * @return String
   */
  private function formatPhoneNumber($phoneNumber)
  {
    //strip +
    $phoneNumber = str_replace("+","",$phoneNumber);

    //strip -
    $phoneNumber = str_replace("-","",$phoneNumber);

    if ($phoneNumber == trim($phoneNumber) && strpos($phoneNumber, ' ') !== false) {

      $exploded = explode(' ',$phoneNumber);

      return $exploded[0].$exploded[1];
    }

    return $phoneNumber;
  }

   protected $fillable = ['product_id', 'product_name', 'name', 'country', 'city', 'postal' , 'address1', 'address2', 'phone', 'sent', 'date','state'];

}
