<?php

namespace App;

use Illuminate\Http\Request;
use App\Events\ProductCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class product extends Model
{

  public function minus_stock($id) {
      $product = product::find($id);
      $product->stock = $product->stock - 1;
      $product->save();
     }

     public function adjust_stock($id, $stock) {
        $product = product::find($id);
        $product->stock = $stock;
        $product->save();
      }

      public function ordered($id) {
         $product = product::find($id);
         $product->orders = $product->orders + 1;
         $product->save();
       }

  public function createProduct(Request $request) {

    $photos = [];

    for ($i=1; $i < 7; $i++) {
      $file = $request->file('photo'.$i);

      if ( $request->hasFile('photo'.$i) && $request->file('photo'.$i)->isValid() ) {
        $extension = $file->extension();
        $name = sha1(time().time().$i).".{$extension}";

        $photos[$i] = $name;

        $file->storeAs('public', $name);
      }
      else {
        return 'error';
      }
    }

    product::create([
      'name' => request('name'),
      'code' => request('code'),
      'photo1' => $photos[1],
      'photo2' => $photos[2],
      'photo3' => $photos[3],
      'photo4' => $photos[4],
      'photo5' => $photos[5],
      'photo6' => $photos[6],
      'price' => request('price'),
      'stock' => 0,
      'orders' => 0
    ]);
    
    // triggers a queue event that optimize uploaded images only if config is set to true
    if (config('optimizations.images')) {
       event(new ProductCreated($photos));
    }
    

    return request('name');

   }


protected $fillable = ['name', 'code', 'photo1', 'photo2', 'photo3', 'photo4', 'photo5', 'photo6','price', 'stock', 'orders'];

}
