<?php

/**
 * Show notification based on session
 *
 * @param String $message
 * @param String $type
 * @return void 
 */
if (!function_exists('show_notification')) {

    function show_notification($message, $type = 'error') {

        //types option are error, success, info, warning

        $notif = view('layouts.notification')->with([
            'message' => $message,
            'type' => $type
        ])->render();

        echo  $notif;
    }
}

/**
 * Set locale session
 *
 * @param String $locale
 * @return void
 */
if (!function_exists('set_active_locale')) {

    function set_active_locale($locale) {
        
        $default_locale = \App::getLocale();

        if (session('locale')) {

            $default_locale = session('locale');
        }

        
        if ($locale === $default_locale) {
            return 'active';
        }
    }
}

/**
 * Get the calling code from this format +30|country
 *
 * @param String $callingCode
 * @return String
 */
if (!function_exists('format_calling_code')) {

    function format_calling_code($callingCode) {

        if (strpos($callingCode,'|')) {

            $callingCode = explode('|',$callingCode);

            return str_replace("+","",$callingCode[0]);
        }
        
       return str_replace("+","",$callingCode);
    }
}

/**
 * Sets dropdown value of calling code based on country
 * Add calling code here when adding country
 *
 * @param $country
 * @return String
 */
if (!function_exists('set_calling_code')) {

    function set_calling_code($country) {

        $country = set_country_name($country);

        $countries = config('dropdowns.callingCode'); 

        if(!array_key_exists($country, $countries)) {

            //this is the default select value
            return '0|Country';
        }

        return $countries[$country].'|'.$country;
    }
}

/**
 * Converts geoplugin country name to select country value
 * Add country list here
 *
 * @param $country
 * @return String
 */
if (!function_exists('set_country_name')) {

    function set_country_name($country) {

        $countries = config('dropdowns.countryNames');

        if (array_key_exists($country,$countries)) {

            return $countries[$country];
        }

        return 'Country';
    }
}

/**
 * Set default postal code based on country
 *
 * @param $country
 * @return String
 */
if (!function_exists('set_postal_code')) {

    function set_postal_code($country) {

        $country = set_country_name($country);

        if ($country === 'Macau' || $country === 'Hong Kong') {
            return '000000';
        }

        return '';
    }
}
