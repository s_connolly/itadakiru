<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\review;
use App\shipments;

class ReviewV extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    private $message;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Calling codes' default value is 0|Code which is why required rule will not work thus using regex to match 0|Code
        return [
            'callingCode' => ['not_regex :/^0\|Code$/i'],
            'phone' => 'required|max:15',
            'comment' => 'required'
        ];
    }

    public function withValidator($validator) {
      $validator->after(function ($validator) {
        if ($this->phoneCheck()) {
          $validator->errors()->add('phone', $this->message);
        }
      });
    }

    public function phoneCheck() {

      $callingCode = explode('|', $this->input('callingCode'));
      $phone = $this->input('phone');
      $reviews = review::where('phone','=',$callingCode[0]." ".$phone)->get();
      $shipment = shipments::where('phone','=',$callingCode[0]." ".$phone)
      ->where('sent','=',1)
      ->get();


      if ($shipment->count()) {

          if ($reviews->count() >= $shipment->count()) {
            $this->message = __('validation.custom.phone.not_allowed');
            return true;
          } else {
            return false;
          }

      } else {
          $this->message = __('validation.custom.phone.not_found');
          return true;
      }

    }



}
