<?php

namespace App\Http\Middleware;

use Closure;
use App\product;


class StockCancel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


      if ($request->input('submit') == 'cancel') {
        return redirect('/');
      }

        return $next($request);
    }
}
