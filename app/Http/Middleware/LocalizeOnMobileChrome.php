<?php

namespace App\Http\Middleware;

use Closure;
use Detection\MobileDetect;
use App\Services\Localization;

class LocalizeOnMobileChrome
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $detect = new MobileDetect;

        if ($detect->isMobile() && $detect->is('Chrome')) {

            Localization::byCountry();
        } else {
            Localization::bySelection();
        }


        return $next($request);
    }
}
