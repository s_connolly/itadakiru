<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Localization;

class LocalizeByCountry
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Localization::byCountry();

        return $next($request);
    }
}
