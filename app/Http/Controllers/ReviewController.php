<?php

namespace App\Http\Controllers;

use App\review;
use App\Http\Requests\ReviewV;
use Illuminate\Http\Request;

class ReviewController extends Controller
{

  public function store(ReviewV $review) {

     //passes thru validator, if passes, then save in model
     //Laravel auto return JSON errors when called with ajax
     //Front end is just an HTML Require

    $new = new review;
    $new->createReview($review);

    return back();
  }


    /* Returns the paginated result which is requested by getReviews function */
    public function getReviews()
    {
      return review::orderBy('created_at','desc')->paginate(5);
    }


}
