<?php

namespace App\Http\Controllers;

use App\product;
use App\review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class IndexController extends Controller
{
  public function index() {

    $reviews = review::orderBy('created_at','desc')->paginate(5);

    if (@$data = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/') ) {
      $json = json_decode($data, true);
      $BTCprice = $json["0"]["price_usd"];
      session(
        ['BTCprice' => [
          'BTCprice'=> $BTCprice
        ]]);

    } else {
      $product = product::orderBy('orders','desc')->get();

      session()->flash("notification",[
        'message' =>  __('notification.btc_api_error'),
        'type' => 'error'
      ]);

      $BTCprice = 1;
      session(
        ['BTCprice' => [
          'BTCprice'=> $BTCprice
        ]]);
        
      return view( 'storefront.index', compact('product', 'BTCprice', 'reviews') );
    }

    $product = product::orderBy('orders','desc')->get();

    return view( 'storefront.index', compact('product', 'BTCprice', 'reviews') );
  }

  public function faq() {
    return view('storefront.faq');
  }



}
