<?php

namespace App\Http\Controllers;

use App\keys;
use Illuminate\Http\Request;

class KeysController extends Controller
{

    public function index()
    {
        return view('admin.geminiKeys');
    }

    public function store(Request $request)
    {


      $validatedData = $request->validate([
        'key' => 'required',
        'secret' => 'required',
      ]);

        $keys = new keys;

        if ( $keys->saveKeys($request) != 1 ) {
          session()->flash("notification",[
            'message' =>  __('notification.notSaved'),
            'type' => 'error'
          ]);
        } else {
          session()->flash("notification",[
            'message' =>  __('notification.keySaved'),
            'type' => 'success'
          ]);
        }


        return back();
    }


    public function destroy(keys $keys)
    {
        //
    }
}
