<?php

namespace App\Http\Controllers;

use App\shipments;
use App\product;
use App\success_denied;

use Illuminate\Http\Request;
use App\blockcypher;


class PaymentController extends Controller
{

  public function check(Request $request, product $product) {

    $rate = $request->input('rate');
    $address = $request->input('address');

    $check = new blockcypher($rate, $address, $request, $product);
    $success_denied = new success_denied($rate, $address, $request, $product);
    $apple = $check->checkIfRecieved();

    if ($apple === true) {
      return $success_denied->success();
    } else {
      return $success_denied->denied($apple);
    }

  }


public function confirmed() {

    $paid = false;

    $paid = session('paid');

    if ( session('confirmed') && ($paid == true) ) {
      $confirmed = session('confirmed');
      return view( 'storefront.confirmed', compact('confirmed') );
    }

    return view( 'storefront.index' );

  }



}
