<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ShippingV;
use App\product;
use App\electrum;
use App\gemini;
use App\keys;
use App\Services\GeoPlugin;
use App\Services\GeoLocation;

class ShippingFormController extends Controller
{

  public function shippingForm(product $product, Request $request) {

    $rate = $request->input('rate');

    $ip = $_SERVER['REMOTE_ADDR'];

    // Gets country based on ip
    try {
      $geo_location = new GeoLocation(new GeoPlugin($ip));
      $country = $geo_location->country();
    } catch (\Exception $e) {
      $country = NULL;
    }



    return view('storefront.shippingForm', compact('product','rate','country'));
  }

/*
1) Validates Form
2) Doubel checks if stock is 0 it redirect to main page
3) Makes it show paid is flase so payment cannot be duplicated
3) Sets form data into a session for future use
4) Make an electrum request
*/

  public function validateShippingForm(ShippingV $request, product $product) { //

    //1
    $validated = $request->validated();

    //2
    if ($product->stock <=0) {
      return redirect('/');
    }

    //2.5
    session(
      ['paid' => [
        'paid'=> false
      ]]);

    //3
    /*Extracting some drop down data*/
    $result_explode = explode('|', $request->input('country'));
    $shipping_rate = (float)$result_explode[0];
    $country = $result_explode[1];

    $result_explode = explode('|', $request->input('callingCode'));
    $callingCode = $result_explode[0];

    //add to session data
    session(
      ['shipping' => [
        'name'=> $request->input('name'),
        'country'=> $country,
        'city'=> $request->input('city'),
        'postal'=> $request->input('postal'),
        'address1'=> $request->input('address1'),
        'address2'=> $request->input('address2'),
        'state' => $request->input('state'),
        'callingCode' => $callingCode,
        'phone' => $request->input('phone')
      ]]);

    //4
    $name = $request->input('name');
    $rate = $request->input('rate');
    //shipping rate set at #3
    $BTCprice = $request->session()->get('BTCprice');

    //should prob be in sep function or class

    $count = keys::count();

    if ($count > 0) {
      $api = keys::getFirstKey();
      $key = $api->key;
      $secret = $api->secret;
    } else {
      session()->flash("notification",[
        'message' =>  __('You have not entered your Gemini API Keys yet'),
        'type' => 'error'
      ]);
    return back();
    }


    if ( !$new = new Gemini($key, $secret) ) {
      session()->flash("notification",[
        'message' =>  __('Error with the Internet, Please try again'),
        'type' => 'error'
      ]);
    return back();
    }

      $address = $new->createAddress();

      if ($address === false) {   //checks if error was sent back
        session()->flash("notification",[
          'message' =>  __('Sorry, Looks like our BTC Exchange is down'),
          'type' => 'error'
        ]);
      return back();
      }


    return view('storefront.payment', compact('product', 'rate', 'address') );

  }

}
