<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocaleController extends Controller
{
    /**
     * Sets locale session
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Request
     */
    public function setLocale(Request $request)
    {
        $locale = $request->input('locale');

        session(['locale' =>  $locale]);

        return redirect()->back();
    }
}


