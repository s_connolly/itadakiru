<?php

/*
switched payment method to Gemini,
but this is here in case someone wants to use an Electrum Wallet on a server
*/


namespace App;

class electrum {

  protected $name, $rate;

  function __construct($name, $rate, $shipping_rate, $BTCprice) {

    $this->name = $this->setNameForElectrum($name);
    $this->rate = $this->setRateForElectrum($rate, $shipping_rate, $BTCprice);

  }

  protected function setNameForElectrum($name) //recives $product->name
  {
    $name = preg_replace('/\s+/', '', $name); //electrum commands cannot have spaces
    return $name;
  }

  protected function setRateForElectrum($rate, $shipping_rate, $BTCprice)
  {
    $shipping = number_format( $shipping_rate / $BTCprice['BTCprice'], 6 );
    $rate = $rate + $shipping;
    return $rate;
  }

  public function getAddress() {


    try {
      $connection = ssh2_connect(env('ssh_ip'), env('ssh_port'));
      $status = ssh2_auth_password($connection, env('ssh_user'), env('ssh_pass'));
    } catch (\Exception $e) {
      session()->flash("notification",[

        'message' =>  __('notification.btc_api_error_e1'),

        'type' => 'error'
      ]);
      return back();
    }

    $stream = ssh2_exec($connection, "electrum addrequest $this->rate -m $this->name --expiration 3600 --force"); //NEED FORCE, NAME cannot hace spaces
    stream_set_blocking($stream, true);
    $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
    $stream_string = stream_get_contents($stream_out);
    $btcinfo = json_decode($stream_string);

    if($btcinfo == NULL) {
      session()->flash("notification",[

        'message' =>  __('notification.btc_api_error_e2'),

        'type' => 'error'
      ]);
      return back();
    }

    ssh2_exec($connection, 'exit');
    unset($connection);

    $address = $btcinfo->address;
    return $address;
  }


}
