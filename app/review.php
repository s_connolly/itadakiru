<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\ReviewV;


class review extends Model
{

    public function createReview(ReviewV $review) {

      $comment = $review->input('comment');
      $callingCode = explode('|',$review->input('callingCode'));
      $phone = $callingCode[0]." ".$review->input('phone');

      review::create([
      'comment' => $comment,
      'phone' => $phone
    ]);

  }
    protected $fillable = ['phone', 'comment'];

}
