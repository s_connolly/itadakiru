<?php

namespace App\Listeners;

use ImageOptimizer;
use App\Events\ProductCreated;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductOptimizeImage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductCreated  $event
     * @return void
     */
    public function handle(ProductCreated $event)
    {
        $photos = $event->photos;

        $path = storage_path().'/app/public/';

        foreach($photos as $photo) {

            try {
                //Optimize images
                ImageOptimizer::optimize($path.$photo);
            } catch(\Exception $e) {
                Log::error($e->getMessage());
            }
        }
    }
}
