<?php

namespace App;
use App\product;
use App\shipments;

use Illuminate\Http\Request;

class success_denied {

  protected $rate, $address, $product, $request;

  function __construct($rate, $address, Request $request, product $product) {
    $this->rate = $rate;
    $this->address = $address;
    $this->product = $product;
    $this->request = $request;
  }


  public function success() {

    if ($paid = session('paid')) {
      if ($paid['paid'] == true ) {
        return redirect('payments/confirmed');
      }
    }

      $new = new shipments;
      $new->createShipment($this->request, $this->product);

      $shipping = session('shipping');
        session(
          ['confirmed' => [
            'product'=> $this->product->name,
            'country'=> $shipping['country'],
            'city'=> $shipping['city']
          ]]);
        session(
          ['paid' => [
            'paid'=> true
          ]]);
      return redirect('payments/confirmed');
  }


  public function denied($recieved) {

    $owed = $this->rate - $recieved;
    $btcinfo = array(
      $owed,
      $this->address,
      $this->rate
    );
    $product = $this->product;
    return view('storefront.owed', compact('product', 'btcinfo') );
  }


}
