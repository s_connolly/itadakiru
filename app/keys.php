<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class keys extends Model
{
  protected $fillable = ['key','secret'];

  public function saveKeys(Request $request) {

    // encrypt with salt
    keys::create([
      'key' => encrypt(request('key')),
      'secret' => encrypt(request('secret'))
    ]);

    return 1;

   }

  // returns key object with decrypted values
  public static function getFirstKey() {

    $api = keys::orderBy('id', 'desc')->first();

    $api->key = decrypt($api->key);

    $api->secret = decrypt($api->secret);

    return $api;
  }
}
