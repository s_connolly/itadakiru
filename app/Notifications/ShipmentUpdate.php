<?php

namespace App\Notifications;

use App;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;

class ShipmentUpdate extends Notification implements ShouldQueue
{
    use Queueable;

    protected $status;

    protected $shipment;

    protected $trackingNumber;

    protected $ip;
    /**
     *
     * @param String $status - can be 'sent' or 'created'
     * @param App\shipments $shipment
     * @param String $trackingNumber
     * @return void
     */
    public function __construct($status, $shipment, $trackingNumber = null)
    {
        $this->status = $status;

        $this->shipment  = $shipment;

        $this->trackingNumber = $trackingNumber;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    /**
     * Handles SMS sending
     *
     * @param mixed  $notifiable
     * @return Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmo($notifiable)
    {

        $message = '';

        $isUnicode = false;

        $country = $this->getCountryFromPhoneNumber($this->shipment->phone);

        // Set locale based on country
        if ( $country === 'Japan') {

            App::setLocale('ja');

            $isUnicode = true;
        }

        elseif ( $country === 'China' || $country === 'Taiwan' || $country === 'Hong Kong') {

            App::setLocale('zh');

            $isUnicode = true;
        }

        else {
            App::setLocale('en');
        }

        // Check if created or sent
        if ( $this->status === 'created') {

            $message = __('sms.shippingCreated',['productName' => $this->shipment->product_name]);
        } else if ( $this->status === 'sent') {
            $message = __('sms.shippingSent',['productName' => $this->shipment->product_name, 'trackingNumber' => $this->trackingNumber]);
        }

        $nexmoMessage = new NexmoMessage;

        // Check if not unicode characters
        if (!$isUnicode) {
            return $nexmoMessage->content($message);
        } else {
            return $nexmoMessage->content($message)->unicode();
        }
    }

    /**
     * Returns country name based on phone number's calling code
     *
     * @param String $phoneNumber
     * @return String
     */
    private function getCountryFromPhoneNumber($phoneNumber)
    {
        $callingCode = explode(" ",$phoneNumber);

        $countries = array_flip(config('dropdowns.callingCode'));

        if (array_key_exists($callingCode[0],$countries)) {

            return $countries[$callingCode[0]];
        }
        // Returns usa as a country to set default value to english
        return 'USA';
    }
}
