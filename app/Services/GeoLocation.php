<?php

namespace App\Services;

use App\Contracts\GeoLocationInterface;

class GeoLocation
{
    private $geo_location; 

    /**
     * Constructor
     *
     * @param  App\Contracts\GeoLocationInterface  $geo_location
     * @return void
     */
    public function __construct(GeoLocationInterface $geo_location)
    {
        $this->geo_location = $geo_location;
    }

    /**
     * Get country name from geolocation data.
     *
     * @return String
     */
    public function country() {
        return $this->geo_location->getCountry();
    }


}