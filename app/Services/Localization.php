<?php

namespace App\Services;

use App;
use App\Services\GeoPlugin;
use App\Services\GeoLocation;

class Localization
{
    /**
     * Localize based on country which uses geolocation api
     *
     * @return void
     */
    public static function byCountry()
    {
        $ip = $_SERVER['REMOTE_ADDR'];


        $geo_location = new GeoLocation(new GeoPlugin($ip));
        $country = $geo_location->country();
        $locale = App::getLocale();

        if(session('locale')) {
            $locale = session('locale');
        } else {

            if ($country !== null) {

                $chinese_countries = [
                    'China',
                    'Hong Kong',
                    'Macao',
                    'Taiwan'
                ];

                if (in_array($country, $chinese_countries)) {

                    $locale = 'zh';
                }

                if ($country === 'Japan') {

                    $locale = 'ja';
                }
            }
        }

        App::setLocale($locale);
    }

    /**
     * Localize based on user selection. By Default localize by browser language
     *
     * @return void
     */
    public static function bySelection()
    {
        $locale = App::getLocale();

        $browser_locale = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        if(session('locale')) {

            $locale = session('locale');
        } else {
            if ($browser_locale === 'zh') {
                $locale = 'zh';
            }

            if ($browser_locale === 'ja') {
                $locale = 'ja';
            }
        }

        App::setLocale($locale);
    }

    /**
     * Localize by browser language
     *
     * @return void
     */
    public static function byBrowser()
    {
        $locale = App::getLocale();

        $browser_locale = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        if ($browser_locale === 'zh') {
            $locale = 'zh';
        }

        if ($browser_locale === 'ja') {
            $locale = 'ja';
        }

        App::setLocale($locale);
    }
}
