<?php

namespace App\Services;

use App\Contracts\GeoLocationInterface;

class GeoPlugin implements GeoLocationInterface
{
    private $url = "http://www.geoplugin.net/php.gp?ip=";

    public $geo_data = [];

    /**
     * Sets geo data from geoplugin.net
     *
     * @param  String $ip
     * @return void
     */
    public function __construct($ip)
    {
        $this->geo_data = unserialize(file_get_contents($this->url.$ip));
    }


    /**
     * Get country name
     *
     * @return String
     */
    public function getCountry()
    {
        return $this->geo_data['geoplugin_countryName'];
    }

}