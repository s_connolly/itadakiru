<?php

//use App\Http\Middleware\stockcancel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/setLocale','LocaleController@setLocale');

Route::middleware(['localizebybrowser'])->group(function(){

    Route::get('/pending', 'AdminController@pending');
    Route::get('/shipped', 'AdminController@shipped');
    Route::get('/manage', 'AdminController@manage');

    Route::POST('/create', 'AdminController@create');
    Route::get('/create', 'AdminController@goHome');

    Route::get('/stock', 'AdminController@stock');
    Route::get('/sent/{id}', 'AdminController@sent');

    Route::get('/delete', 'AdminController@delete');
    Route::get('/productDelete/{id}', 'AdminController@productDelete');

    Route::get('/geminiKeys','KeysController@index');
    Route::POST('/geminiKeys/store','KeysController@store');
});

Route::middleware(['localizeonmobilechrome'])->group(function(){


    Route::get('/bluelogin', 'Auth\loginController@loginform')->name('loginform')->middleware('exists');                         //login form
    Route::POST('/bluelogin', 'Auth\loginController@login')->name('login')->middleware('exists');                                             //login in attempt
    Route::get('/logout', 'Auth\loginController@logout');

    Route::POST('/register', 'Auth\registerController@register')->middleware('notexist');
    Route::get('/register', 'Auth\registerController@registerform')->middleware('notexist');

    Route::get('/', 'IndexController@index')->middleware('exists');   //Homepage
    Route::get('/shippingForm/{product}', 'ShippingFormController@shippingForm');          //ShippingForm
    Route::get('/payment/{product}', 'ShippingFormController@validateShippingForm')->middleware('stockcancel');  //Checks Shipping Form

    Route::get('/checkpayment/{product}', 'PaymentController@check');    //PAYMENT CHECK
    Route::get('/payments/confirmed', 'PaymentController@confirmed'); //needs to be paymentS so it doenst clash with payment/{product}

    Route::get('/faq','IndexController@faq');

    Route::POST('/store', 'ReviewController@store');
    Route::get('/reviews/getReviews','ReviewController@getReviews');

});
