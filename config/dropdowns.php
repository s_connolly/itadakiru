<?php 

return [
    // List of Countries with their calling codes
    'callingCode' => [
        //'USA' => '+1',
        'Japan' => '+81',
        'Hong Kong' => '+852',
        'Taiwan' => '+886',
        'Macau' => '+853',
        'China' => '+86'
    ],
    // Geoplugin api returns different country names like Macao instead of Macau.
    // This array is used to convert it to country name that is in the dropdown 
    'countryNames' => [
        //'United States' => 'USA',
        'Japan' => 'Japan',
        'Hong Kong' => 'Hong Kong',
        'Taiwan' => 'Taiwan',
        'Macao' => 'Macau',
        'China' => 'China'
    ]
];